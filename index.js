var TxtType = function(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function() {
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

    var that = this;
    var delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
    }

    setTimeout(function() {
        that.tick();
    }, delta);
};

window.onload = function() {

    var knowMoreButtons = document.getElementsByClassName("know-more");
    var knowMoreWhiteButtons = document.getElementsByClassName("know-more-white");
    var crossButtons = document.getElementsByClassName("cross-symbol");
    var plusButtons = document.getElementsByClassName("plus-symbol");
    var backToTopButton = document.getElementsByClassName("back-to-top");

    for (var i = 0; i < knowMoreButtons.length; i++) {
        knowMoreButtons[i].addEventListener("click", display);
    }
    for (var i = 0; i < knowMoreWhiteButtons.length; i++) {
        knowMoreWhiteButtons[i].addEventListener("click", display);
    }
    for (var i = 0; i < crossButtons.length; i++) {
        crossButtons[i].addEventListener("click", reset);
    }
    for (var i = 0; i < plusButtons.length; i++) {
        plusButtons[i].addEventListener("click", display);
    }
    backToTopButton[0].addEventListener("click", backToTop)


    var elements = document.getElementsByClassName('typewrite');
    for (var i=0; i<elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
            new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { padding: 0.25em; height: 0.5em; border-right: 0.01em solid gray;}";
    document.body.appendChild(css);
};

let element1 = document.getElementsByClassName("directly-visible");
let button1 = document.getElementsByClassName("know-more button1");
let button2 = document.getElementsByClassName("know-more button2");
let button3 = document.getElementsByClassName("know-more button3");
let button4 = document.getElementsByClassName("know-more button4");
let button5 = document.getElementsByClassName("know-more-white button5");
let button6 = document.getElementsByClassName("know-more-white button6");
let button7 = document.getElementsByClassName("know-more-white button7");
let button8 = document.getElementsByClassName("know-more-white button8");
let button9 = document.getElementsByClassName("plus-symbol button9");
let button10 = document.getElementsByClassName("plus-symbol button10");
let button11 = document.getElementsByClassName("plus-symbol button11");
let button12 = document.getElementsByClassName("plus-symbol button12");
let button13 = document.getElementsByClassName("plus-symbol button13");


let tile1 = document.getElementsByClassName("card back-content-real-estate");
let tile2 = document.getElementsByClassName("card back-content-otan");
let tile3 = document.getElementsByClassName("card back-content-semester");
let tile4 = document.getElementsByClassName("card back-content-retail");
let tile5 = document.getElementsByClassName("card back-content-trading-bot");
let tile6 = document.getElementsByClassName("card back-content-boy-scout");
let tile7 = document.getElementsByClassName("card back-content-sales");
let tile8 = document.getElementsByClassName("card back-content-cambridge");
let tile9 = document.getElementsByClassName("card back-content-values");
let tile10 = document.getElementsByClassName("card back-content-me");
let tile11 = document.getElementsByClassName("card back-content-sport");
let tile12 = document.getElementsByClassName("card back-content-creations");
let tile13 = document.getElementsByClassName("card back-content-culture");

let close1 = document.getElementsByClassName("cross-symbol close1");
let close2 = document.getElementsByClassName("cross-symbol close3");
let close3 = document.getElementsByClassName("cross-symbol close5");
let close4 = document.getElementsByClassName("cross-symbol close6");
let close5 = document.getElementsByClassName("cross-symbol close2");
let close6 = document.getElementsByClassName("cross-symbol close7");
let close7 = document.getElementsByClassName("cross-symbol close4");
let close8 = document.getElementsByClassName("cross-symbol close8");
let close9 = document.getElementsByClassName("cross-symbol close9");
let close10 = document.getElementsByClassName("cross-symbol close10");
let close11 = document.getElementsByClassName("cross-symbol close11");
let close12 = document.getElementsByClassName("cross-symbol close12");
let close13 = document.getElementsByClassName("cross-symbol close13");

let contentTiles = [tile1, tile2, tile3, tile4, tile5, tile6, tile7, tile8, tile9, tile10, tile11, tile12, tile13];
let closeButtons = [close1, close2, close3, close4, close5, close6, close7, close8, close9, close10, close11, close12, close13];

function display(){
    console.log(this);
    element1[0].style.filter = "blur(50px)";
    switch (this) {
        case button1[0]:
            tile1[0].style.display = "block";
            break;
        case button2[0]:
            tile2[0].style.display = "block";
            break;
        case button3[0]:
            tile3[0].style.display = "block";
            break;
        case button4[0]:
            tile4[0].style.display = "block";
            break;
        case button5[0]:
            tile5[0].style.display = "block";
            break;
        case button6[0]:
            tile6[0].style.display = "block";
            break;
        case button7[0]:
            tile7[0].style.display = "block";
            break;
        case button8[0]:
            tile8[0].style.display = "block";
            break;
        case button9[0]:
            tile9[0].style.display = "block";
            break;
        case button10[0]:
            tile10[0].style.display = "block";
            break;
        case button11[0]:
            tile11[0].style.display = "block";
            break;
        case button12[0]:
            tile12[0].style.display = "block";
            break;
        case button13[0]:
            tile13[0].style.display = "block";
            break;
    }
}

function reset(){
    for(let i=0; i<contentTiles.length; i++){
        if(contentTiles[i][0].style.display === "block"){
            if (this === closeButtons[i][0]){
                contentTiles[i][0].style.display = "none";
                element1[0].style.filter = "blur(0px)";
            }
        }
    }
}

window.addEventListener("scroll", appear);

function appear(){
    if (document.documentElement.scrollTop > 100)
    {document.getElementsByClassName("back-to-top")[0].style.display = "block"}
    else
    {document.getElementsByClassName("back-to-top")[0].style.display = "none"}
}

function backToTop(){
    document.body.scrollIntoView({
        behavior: "smooth",
    });
}